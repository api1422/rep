package testdemo;
import static org.junit.Assert.assertEquals;

import  org.junit.*;
//import  static org.junit.Assert.*;
public class Example {
	static EvenOdd obj;
	static Prime ob;
	@BeforeClass
	public static  void avc1()
	{
		
		obj=new EvenOdd();
		ob=new Prime();
		System.out.println("I am BeforeAll..");
	}
	@Before
	public  void avc2()
	{
		System.out.println("I am BeforeEach");
		
	}
	@Test
	public void avc3()
	{
		assertEquals(obj.evenodd(100),true);
		System.out.println("I am TestCAse");
		
	}
	@AfterClass
	public static void avc4()
	{
		System.out.println("I am BeforeEach");
		
	}
	@AfterClass
	public static void avc5()
	{
		System.out.println("I am AfterAll");
		
	}
	@Test
	public void avc6()
	{
		assertEquals(obj.evenodd(101),false);
		System.out.println("I am TestCAse");
		
	}
	
	@Test
	public void avc7()
	{
		assertEquals(ob.isPrime(7),true);
		System.out.println("I am TestCAse");
		
	}
	@Test
	public void avc8()
	{
		assertEquals(ob.isPrime(10),false);
		System.out.println("I am TestCAse");
		
	}
	
	

}
